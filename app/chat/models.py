from datetime import datetime

from sqlalchemy import Column, Integer, String, \
    Boolean, ForeignKey, DateTime
from sqlalchemy.orm import relationship

from settings.database import Base


class Message(Base):
    __tablename__ = "messages"

    id = Column(Integer, primary_key=True, index=True)
    body = Column(String, index=True)
    is_read = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.utcnow)
    read_at = Column(DateTime, nullable=True, default=None)
    owner_id = Column(Integer, ForeignKey("users.id"))
    receiver_id = Column(Integer, ForeignKey("users.id"))
    room_id = Column(Integer, ForeignKey('chat_rooms.id'))

    owner = relationship("User", primaryjoin="Message.owner_id==User.id")
    receiver = relationship("User", primaryjoin="Message.receiver_id==User.id")
    room = relationship("ChatRoom", primaryjoin="Message.room_id==ChatRoom.id")


class ChatRoom(Base):
    __tablename__ = "chat_rooms"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String, index=True)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship("User", primaryjoin="ChatRoom.owner_id==User.id")
