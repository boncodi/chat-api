from datetime import datetime

from sqlalchemy.orm import Session

from app.chat.models import Message, ChatRoom
from app.chat.schemas import MessageCreateScheme, ChatRoomCreateScheme


def create_message(db: Session, data: MessageCreateScheme) -> Message:
    db_message = Message(**data.dict())
    db.add(db_message)
    db.commit()
    db.refresh(db_message)
    return db_message


def get_message(db: Session, message_id):
    return db.query(Message).filter(Message.id == message_id).first()


def get_messages(db: Session, room_uuid: str):
    return db.query(Message).filter(Message.room.has(uuid=room_uuid)).order_by(Message.created_at.desc())


def get_unread_message_count(db: Session, receiver_id: int, room_id: int) -> int:
    return db.query(Message).filter(Message.receiver_id == receiver_id, Message.room_id == room_id,
                                    Message.is_read == False).count()


def get_chat_room_by_uuid(db: Session, uuid: str) -> ChatRoom:
    return db.query(ChatRoom).filter(ChatRoom.uuid == uuid).first()


def set_read_messages(db: Session, receiver_id: int, message_id: int, date_time: datetime):
    message = db.query(Message).filter(Message.receiver_id == receiver_id, Message.id == message_id). \
        update({'is_read': 1, 'read_at': date_time}, synchronize_session="fetch")
    db.commit()
    db.refresh(message)
    return message


def create_chat_room(db: Session, data: ChatRoomCreateScheme) -> ChatRoom:
    db_message = ChatRoom(**data.dict())
    db.add(db_message)
    db.commit()
    db.refresh(db_message)
    return db_message
