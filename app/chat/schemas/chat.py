from datetime import datetime

from pydantic import BaseModel, Field

from app.auth.schemas import UserDetailSchema


class ChatRoomCreateScheme(BaseModel):
    uuid: str = Field()
    owner_id: int = Field()


class ChatRoomDetailScheme(ChatRoomCreateScheme):
    id: int = Field()
    owner: UserDetailSchema = dict()

    class Config:
        orm_mode = True


class MessageCreateScheme(BaseModel):
    body: str
    receiver_id: int
    created_at: datetime
    owner_id: int
    room_id: int


class MessageDetailScheme(BaseModel):
    id: int
    body: str
    receiver_id: int
    created_at: datetime
    owner_id: int
    room_id: int
    is_read: bool
    room: ChatRoomDetailScheme = dict()
    owner: UserDetailSchema = dict()

    class Config:
        orm_mode = True
