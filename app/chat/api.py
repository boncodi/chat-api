import json
import uuid
from datetime import datetime

from fastapi import Depends, APIRouter, WebSocket, WebSocketDisconnect
from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.orm import Session

from app.auth.api import get_current_user
from app.auth.security import verify_ws
from app.chat.schemas import ChatRoomCreateScheme, ChatRoomDetailScheme, MessageCreateScheme, MessageDetailScheme
from app.chat.services import create_chat_room, get_chat_room_by_uuid, create_message, get_unread_message_count, \
    get_messages, set_read_messages, get_message
from app.chat.ws import connection_manager
from settings.pagination import Page
from settings.services import get_db

router = APIRouter(prefix='/chat', tags=['chat'])


@router.post("/rooms/", response_model=ChatRoomDetailScheme)
async def create_room(current_user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    chat_room = ChatRoomCreateScheme(
        uuid=str(uuid.uuid1()),
        owner_id=current_user['id']
    )
    return create_chat_room(db=db, data=chat_room)


@router.get("/messages/{room_uuid}", response_model=Page[MessageDetailScheme])
async def get_messages_list(room_uuid: str, db: Session = Depends(get_db)):
    return paginate(get_messages(db=db, room_uuid=room_uuid))


@router.post("/messages/{message_id}/set-read", response_model=MessageDetailScheme)
async def set_message_read(message_id: int, current_user: dict = Depends(get_current_user),
                           db: Session = Depends(get_db)):
    message = get_message(db=db, message_id=message_id)
    if not message.is_read:
        set_read_messages(db=db, receiver_id=current_user["id"], message_id=message_id, date_time=datetime.utcnow())
    return message


@router.get("/messages/{room_uuid}/unread")
async def get_unread_messages(current_user: dict = Depends(get_current_user), db: Session = Depends(get_db),
                              room_uuid: str = None):
    receiver_id = current_user["id"]
    chat_room = get_chat_room_by_uuid(db=db, uuid=room_uuid)
    unread_message_count = get_unread_message_count(
        db=db,
        receiver_id=receiver_id,
        room_id=chat_room.id
    )
    return {
        "count": unread_message_count
    }


@router.websocket("/ws/{room_uuid}/{receiver_id}")
async def websocket_endpoint(
        websocket: WebSocket,
        db: Session = Depends(get_db),
        user_data: dict = Depends(verify_ws),
        room_uuid: str = None,
        receiver_id: int = None
):
    owner_id = user_data["id"]
    chat_room = get_chat_room_by_uuid(db=db, uuid=room_uuid)

    await connection_manager.connect(websocket, room_uuid)
    try:
        while True:
            body = await websocket.receive_text()
            room_members = (
                connection_manager.get_members(room_uuid)
                if connection_manager.get_members(room_uuid) is not None
                else []
            )
            if websocket not in room_members:
                await connection_manager.connect(websocket, room_uuid)

            message_create_schema = MessageCreateScheme(
                body=body,
                receiver_id=receiver_id,
                owner_id=owner_id,
                room_id=chat_room.id,
                created_at=datetime.now()
            )
            message = create_message(db=db, data=message_create_schema)
            message_detail_schema = MessageDetailScheme.from_orm(message)
            await connection_manager.notify(message_detail_schema.json(), room_uuid)
    except WebSocketDisconnect:
        connection_manager.remove(websocket, room_uuid)
