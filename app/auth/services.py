from sqlalchemy.orm import Session

from app.auth.models import User
from app.auth.schemas import UserCreateSchema, UserLoginSchema
from app.auth.utils import get_password_hash


def create_user(db: Session, user: UserCreateSchema):
    db_user = User()
    db_user.first_name = user.first_name
    db_user.last_name = user.last_name
    db_user.is_active = True
    db_user.email = user.email
    db_user.password = get_password_hash(user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user(db: Session, user: UserLoginSchema) -> User | None:
    return db.query(User).filter(User.email == user.email).first()


def get_user_by_id(db: Session, user_id: int) -> User:
    return db.query(User).filter(User.id == user_id).first()


def get_users(db: Session, exclude: int):
    return db.query(User).filter(User.id != exclude).all()
