import json
from typing import Any

from fastapi import Depends, APIRouter, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from fastapi_pagination import paginate
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

from app.auth.auth_bearer import JWTBearer
from app.auth.schemas import UserCreateSchema, UserDetailSchema, UserLoginSchema
from app.auth.services import create_user, get_user, get_user_by_id, get_users
from app.auth.utils import verify_password, create_JWT, refresh_JWT, decode_JWT
from settings.pagination import Page
from settings.services import get_db

router = APIRouter(prefix='/auth', tags=['auth'])

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/sign-in")


async def get_current_user(token: str = Depends(oauth2_scheme)):
    user = decode_JWT(token)
    return json.loads(user['sub'])


@router.post("/sign-up")
async def sign_up(user: UserCreateSchema, db: Session = Depends(get_db)) -> Any:
    try:
        user = create_user(db=db, user=user)
    except IntegrityError:
        return HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="User with this email already exists")
    return UserDetailSchema.from_orm(user)


@router.post("/sign-in")
async def sign_in(user: UserLoginSchema, db: Session = Depends(get_db)):
    user_db = get_user(db=db, user=user)
    wrong_response = HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Email or password are wrong")
    if user_db:
        if verify_password(user.password, user_db.password):
            user_data = UserDetailSchema.from_orm(user_db).json()
            return {
                "access_token": create_JWT(user_data),
                "refresh_token": refresh_JWT(user_data)
            }
        else:
            return wrong_response
    return wrong_response


@router.get("/me", dependencies=[Depends(JWTBearer()), Depends(oauth2_scheme)], response_model=UserDetailSchema)
async def sign_in(current_user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    return get_user_by_id(db=db, user_id=current_user['id'])


@router.get("/users", dependencies=[Depends(JWTBearer()), Depends(oauth2_scheme)],
            response_model=Page[UserDetailSchema])
async def users(current_user: dict = Depends(get_current_user), db: Session = Depends(get_db)):
    return paginate(get_users(db=db, exclude=current_user['id']))
