import json

from fastapi import WebSocket, status, Query

from app.auth.utils import verify_jwt, decode_JWT


async def verify_ws(
    websocket: WebSocket,
    token: str = Query(default=None)
):
    if not token or not verify_jwt(token):
        await websocket.close(code=status.WS_1008_POLICY_VIOLATION)

    user = decode_JWT(token)
    return json.loads(user['sub'])
