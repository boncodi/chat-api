from pydantic import BaseModel, Field, EmailStr


class UserCreateSchema(BaseModel):
    first_name: str = Field()
    last_name: str = Field()
    email: EmailStr = Field()
    password: str = Field()

    class Config:
        schema_extra = {
            "example": {
                "first_name": "John",
                "last_name": "Doe",
                "email": "yourmail@yourdomain.com",
                "password": "your_password"
            }
        }


class UserDetailSchema(BaseModel):
    id: int = Field()
    first_name: str = Field()
    last_name: str = Field()
    email: EmailStr = Field()

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": 1,
                "first_name": "John",
                "last_name": "Doe",
                "email": "yourmail@yourdomain.com",
            }
        }


class UserLoginSchema(BaseModel):
    email: EmailStr = Field()
    password: str = Field()

    class Config:
        schema_extra = {
            "example": {
                "email": "yourmail@yourdomain.com",
                "password": "your_password"
            }
        }
