from fastapi import FastAPI, Depends, APIRouter
from fastapi_pagination import add_pagination
from fastapi.middleware.cors import CORSMiddleware

from app.auth.api import router as auth_router, oauth2_scheme
from app.chat.api import router as chat_router
from app.auth.auth_bearer import JWTBearer

from settings.services import create_database

app = FastAPI()
root_api = APIRouter(prefix="/api/v1")

origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

root_api.include_router(chat_router, dependencies=[Depends(JWTBearer()), Depends(oauth2_scheme)])
root_api.include_router(auth_router)
app.include_router(root_api)

create_database()

add_pagination(app)

