FROM python:3.10.0rc1-bullseye

ENV PYTHONUNBUFFERED 1

ENV PROJECT_NAME chat-app
ENV PROJECT_PATH /var/www/$PROJECT_NAME

RUN mkdir -p $PROJECT_PATH

WORKDIR $PROJECT_PATH

# Copy requirements for catch
ADD ./requirements.txt $PROJECT_PATH
RUN pip3 install -r requirements.txt

# Copy project files
ADD . $PROJECT_PATH
